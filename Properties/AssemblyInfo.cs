﻿using System.Resources;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;

// General Information about an assembly is controlled through the following
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("RPSclub-App")]
[assembly: AssemblyDescription("A simple application for Rock Paper Science Club")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Heriot-Watt University")]
[assembly: AssemblyProduct("RPSclub Application")]
[assembly: AssemblyCopyright("Carbonate Group, IPE, HW 2018")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]

// Setting ComVisible to false makes the types in this assembly not visible
// to COM components.  If you need to access a type in this assembly from
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]

// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("1582c160-c7a0-47ea-8c7c-7b7c9f97135a")]

// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version
//      Build Number
//      Revision
// 
// You can specify all the values or you can default the Build and Revision Numbers
// by using the '*' as shown below:
// [assembly: AssemblyVersion("1.0.*")]
[assembly: AssemblyVersion("2.5.*")]
[assembly: AssemblyFileVersion("2.5.0.0")]
[assembly: NeutralResourcesLanguage("en-GB")]

