﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace RPSclub
{
    public partial class Form2 : Form
    {
        public Form1 ParentForm1 { get; set; }
        private MyTime counter;
        MyTime zero = new MyTime(0);

        public Form2(Form1 parent, int seconds)
        {
            InitializeComponent();
            ParentForm1 = parent;
            counter = new MyTime(seconds);
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            label1.Text = counter.ToString();
            timer1.Interval = 1000; // every 1000 milli second
            timer1.Enabled = true;
            timer1.Start();
        }

        private void Form2_FormClosing(object sender, FormClosingEventArgs e)
        {
            timer1.Stop();
            ParentForm1.WindowState = FormWindowState.Normal;
            ParentForm1.Focus();
        }

        private void Timer1_Tick(object sender, EventArgs e)
        {
            label1.Text = (--counter).ToString();
            if (counter <= zero)
            {
                this.Size = new Size(196, 188);
                label2.Visible = true;
                button1.Visible = true;
                label1.ForeColor = Color.Red;
                label1.Location = new Point(33, 50);
                label1.Text = counter.ToString();
            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            this.Close();
            ParentForm1.WindowState = FormWindowState.Normal;
            ParentForm1.Focus();
        }
    }
    /// <summary>
    /// To have a class that prints out the time in the form mm:ss
    /// </summary>
    public class MyTime
    {
        public int Seconds{ get; set; }
        public MyTime(int seconds)
        {
            Seconds = seconds;
        }
        public override string ToString()
        {
            if (Seconds > 0)
            {
                int min = Seconds / 60;
                int sec = Seconds - min * 60;
                return (min.ToString("00") + ":" + sec.ToString("00"));
            }
            else if (Seconds ==0)
                return "00:00";
            else
            {
                int min = (Seconds / 60);
                int sec = (Seconds - min * 60);
                return (min.ToString("00") + ":" + (-sec).ToString("00"));
            }
        }
        public static MyTime operator --(MyTime t)
        {
            t.Seconds--;
            return t;
        }
        public static bool operator <=(MyTime t1,MyTime t2)
        {
            return (t1.Seconds <= t2.Seconds)? true : false;
        }
        public static bool operator >=(MyTime t1, MyTime t2)
        {
            return (t1.Seconds >= t2.Seconds) ? true : false;
        }
        public static bool operator ==(MyTime t1, MyTime t2)
        {
            return (t1.Seconds <= t2.Seconds) ? true : false;
        }
        public static bool operator !=(MyTime t1, MyTime t2)
        {
            return (t1.Seconds != t2.Seconds) ? true : false;
        }
    }
}
