﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing.Imaging;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Diagnostics;

namespace RPSclub
{
    public partial class Form1 : Form
    {
        bool aFileOpened = false;
        string[] file_address, journalList, nameList, file_name;
        int time ; // time of each presenation in seconds
        public Form1()
        {
            InitializeComponent();
            string month = System.DateTime.Today.Month.ToString("00");
            string day = System.DateTime.Today.Day.ToString("00");
            string year = System.DateTime.Today.Year.ToString("0000");
            string today_folder = month + '_' + day + '_' + year;
            label1.Text = today_folder;
            textBox1.Text = @"\\petfiler\rpsclub$\papers\" + today_folder;
            nTextBox.Text = @"\\petfiler\rpsclub$\participants.txt";
            jTextBox.Text = @"\\petfiler\rpsclub$\journals.txt";

            // adding this line to test the app, remove when finished
            //textBox1.Text = @"\\petfiler\Carbonates\RPSClub\papers\" + "04_12_2018";

            File_loader(jTextBox.Text, nTextBox.Text);
            
        }


        /// <summary>
        /// Turns a DataGridView to an equivalent text string. All the divisions and modes are to format it like a table.
        /// </summary>
        /// <param name="table"></param>
        /// <returns></returns>
        private string Table2Text(DataGridView table)
        {
            string output = "";
            int col0 = 0, col1 = 0, col2 = 0;
            string c0, c1, c2;
            /* This loop finds the maximum string in each column */
            for (int i = 0; i < table.Rows.Count; i++)
            {
                if ((table.Rows[i].Cells[0].Value != null) && (table.Rows[i].Cells[0].Value.ToString().Length > col0))
                    col0 = table.Rows[i].Cells[0].Value.ToString().Length;
                if ((table.Rows[i].Cells[1].Value != null) && (table.Rows[i].Cells[1].Value.ToString().Length > col1))
                    col1 = table.Rows[i].Cells[1].Value.ToString().Length;
                if ((table.Rows[i].Cells[2].Value != null) && (table.Rows[i].Cells[2].Value.ToString().Length > col2))
                    col2 = table.Rows[i].Cells[2].Value.ToString().Length;
            }
            /* This loops create the table given that a monospace font is used. */
            for (int i = 0; i < table.Rows.Count; i++)
            {
                int j = 0;
                c0 = (table.Rows[i].Cells[0].Value != null) ? table.Rows[i].Cells[0].Value.ToString() : "";
                c1 = (table.Rows[i].Cells[1].Value != null) ? table.Rows[i].Cells[1].Value.ToString() : "[Choose a different journal]";
                c2 = (table.Rows[i].Cells[2].Value != null) ? table.Rows[i].Cells[2].Value.ToString() : "[Choose a different journal]";
                output += c0;
                for (j = 0; j < col0 - c0.Length; j++) output += " ";
                output += "\t" + c1;
                for (j = 0; j < col1 - c1.Length; j++) output += " ";
                output += "\t" + c2;
                for (j = 0; j < col2 - c2.Length; j++) output += " ";
                output += "\n\n";
            }
            return output;
        }
        private int Div8(int a) { return a / 7; }
        private int Mod8(int a) { return a - Div8(a) * 7; }

        /// <summary>
        /// Turns on fly text to image
        /// </summary>
        /// <param name="text"></param>
        /// <param name="font"></param>
        /// <param name="textColor"></param>
        /// <param name="backColor"></param>
        /// <returns></returns>
        private Image DrawText(String text, Font font, Color textColor, Color backColor)
        {
            //first, create a dummy bitmap just to get a graphics object
            Image img = new Bitmap(1, 1);
            Graphics drawing = Graphics.FromImage(img);

            //measure the string to see how big the image needs to be
            SizeF textSize = drawing.MeasureString(text, font);

            //free up the dummy image and old graphics object
            img.Dispose();
            drawing.Dispose();

            //create a new image of the right size
            img = new Bitmap((int)textSize.Width, (int)textSize.Height);

            drawing = Graphics.FromImage(img);

            //paint the background
            drawing.Clear(backColor);

            //create a brush for the text
            Brush textBrush = new SolidBrush(textColor);

            drawing.DrawString(text, font, textBrush, 0, 0);

            drawing.Save();

            textBrush.Dispose();
            drawing.Dispose();

            return img;

        }

        // This function is not called
        /// <summary>
        /// Capture the screen using User32 class
        /// </summary>
        /// <param name="procName"></param>
        public void CaptureApplication(string procName)
        {
            var proc = Process.GetProcessesByName(procName)[0];
            var rect = new User32.Rect();
            User32.GetWindowRect(proc.MainWindowHandle, ref rect);

            int width = rect.right - rect.left;
            int height = rect.bottom - rect.top;

            var bmp = new Bitmap(width, height, PixelFormat.Format32bppArgb);
            Graphics graphics = Graphics.FromImage(bmp);
            graphics.CopyFromScreen(rect.left, rect.top, 0, 0, new Size(width, height), CopyPixelOperation.SourceCopy);
            SaveFileDialog sfd = new SaveFileDialog();
            Stream myStream;
            string addr;
            sfd.Title = "Save the screenshot";
            sfd.Filter = "png files (*.png)|*.png";
            sfd.RestoreDirectory = true;
            if (sfd.ShowDialog() == DialogResult.OK)
            {
                try
                {
                    if ((myStream = sfd.OpenFile()) != null)
                    {
                        addr = sfd.FileName;
                        // Code to write the stream goes here.
                        myStream.Close();
                        bmp.Save(addr, ImageFormat.Png);
                    }
                    else
                        MessageBox.Show("The file was not saved!");
                }
                catch(Exception)
                {
                    MessageBox.Show("Unknown exception occured!");
                }
            }

            //bmp.Save("c:\\tmp\\test.png", ImageFormat.Png);
            
        }

        /// <summary>
        /// Timer start
        /// </summary>
        /// <param name="sender">sender object</param>
        /// <param name="e">e EventArgs</param>
        private void Start_Click(object sender, EventArgs e)
        {
            time = (int) (60 * minutesUpDown.Value + secondsUpDown.Value);
            Form2 form2 = new Form2(this, time);
            this.TopMost = false;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.WindowState = FormWindowState.Minimized;
            form2.Show();
            form2.WindowState = FormWindowState.Normal;
        }
        
        /// <summary>
        /// Folder load
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Load_Click(object sender, EventArgs e)
        {
            try
            {
                file_address = Directory.GetFiles(textBox1.Text, "*.pdf");
                new Random().Shuffle<string>(file_address);
                listBox1.Items.Clear();
                file_name = new string[file_address.Length];
                for (int i = 0; i < file_address.Length; i++)
                {
                    file_name[i] = file_address[i].Substring(file_address[i].LastIndexOf('\\') + 1);
                    listBox1.Items.Add((i + 1).ToString() + "   " + file_name[i]);
                }
            }
            catch (Exception)
            {
                System.Windows.Forms.MessageBox.Show("Could not load the specified folder", "Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// PDF reader
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void File_Opener_Click(object sender, EventArgs e)
        {
            try
            {
                string file = listBox1.SelectedItem.ToString();
                System.Diagnostics.Process.Start("AcroRd32", '\"' + file_address[listBox1.SelectedIndex] + '\"');
                this.TopMost = true;
                this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            }
            catch (Exception)
            { }
        }

        /// <summary>
        /// Previewer
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Previewer_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                label1.Text = (file_name[listBox1.SelectedIndex].Length > 40) ?
                    (file_name[listBox1.SelectedIndex].Substring(0, 40) + "...") :
                    (file_name[listBox1.SelectedIndex]);
            }
            catch (Exception)
            { }
        }

        /// <summary>
        /// Shuffling names and journals
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Shuffler_Click(object sender, EventArgs e)
        {
            //Initialising
            nameList = GetNames();
            journalList = GetJournals();

            if (dataGridView1.Rows.Count==0)
                dataGridView1.Rows.Add(nameList.Length);

            //shuffle
            new Random().Shuffle<string>(nameList);
            new Random().Shuffle<string>(journalList);

            // assign and show
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
                dataGridView1.Rows[i].Cells["names_shuffle"].Value = nameList[i];
            for (int i = 0; i < Math.Min(journalList.Length,dataGridView1.Rows.Count); i++)
                dataGridView1.Rows[i].Cells["journal_shuffle1"].Value = journalList[i];
            if (journalList.Length > nameList.Length)
                for (int i = 0; (i < journalList.Length - nameList.Length) & (i < dataGridView1.Rows.Count)
                    ; i++)
                    dataGridView1.Rows[i].Cells["journal_shuffle2"].Value = 
                        journalList[i + nameList.Length];
        }

        /// <summary>
        /// Load participants from the file
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Participants_Load_Click(object sender, EventArgs e)
        {
            File_loader(jTextBox.Text, nTextBox.Text);
        }

        /// <summary>
        /// Take a screenshot Click
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Capture_Click(object sender, EventArgs e)
        {
            Font myfont= new Font("Consolas", 15.75F, FontStyle.Regular, GraphicsUnit.Point, ((byte)(0)));
            Image tbs = DrawText(Table2Text(dataGridView1), myfont, Color.Black, Color.White);
            Stream myStream;
            string adr;
            SaveFileDialog sfd = new SaveFileDialog()
            {
                Title = "Save the screenshot",
                Filter = "png file (*.png)|*.png",
                RestoreDirectory = true
            };
            if (sfd.ShowDialog()==DialogResult.OK)
            {
                try
                {
                    if ((myStream = sfd.OpenFile()) != null)
                    {
                        adr = sfd.FileName;
                        myStream.Close();
                        tbs.Save(adr, ImageFormat.Png);
                    }
                    else
                        MessageBox.Show("The file was not save for unknown reason");
                }
                catch (Exception)
                {
                    MessageBox.Show("The file was not save for unknown reason");
                }
            }
            // This functionaly was removed
            //CaptureApplication("RPSclub");
        }

        /// <summary>
        /// Get the name of participants from the third tab
        /// </summary>
        /// <returns></returns>
        private string[] GetNames()
        {            
            int NRows = dataGridView2.Rows.Count;
            foreach (DataGridViewRow item in dataGridView2.Rows)
                if ((item.Cells["names"].Value == null)
                    || (item.Cells["names"].Value.ToString().Trim(' ') == ""))
                    NRows--;
            string[] output = new string[NRows];
            for (int i = 0; i < NRows; i++)
            {
                if ((dataGridView2.Rows[i].Cells["names"].Value != null)
                    && (dataGridView2.Rows[i].Cells["names"].Value.ToString().Trim(' ') != ""))
                {
                    output[i] = dataGridView2.Rows[i].Cells["names"].Value.ToString().Trim(' ');
                }
            }
            return output;
        }

        /// <summary>
        /// Get the name of journals from the 3rd tab
        /// </summary>
        /// <returns></returns>
        private string[] GetJournals()
        {
            int NRows = dataGridView2.Rows.Count;
            foreach (DataGridViewRow item in dataGridView2.Rows)
                if ((item.Cells["journals"].Value == null)
                    || (item.Cells["journals"].Value.ToString().Trim(' ') == ""))
                    NRows--;
            string[] output = new string[NRows];
            for (int i = 0; i < NRows; i++)
            {
                if ((dataGridView2.Rows[i].Cells["journals"].Value != null)
                    && (dataGridView2.Rows[i].Cells["journals"].Value.ToString().Trim(' ') != ""))
                {
                    output[i] = dataGridView2.Rows[i].Cells["journals"].Value.ToString().Trim(' ');
                }
            }
            return output;
        }

        /// <summary>
        /// File loader routine
        /// </summary>
        /// <param name="which"></param>
        /// <param name="address"></param>
        /// <param name="whichlabel"></param>
        private void File_loader(string journal_Address, string name_Address)
        {
            dataGridView2.Rows.Clear();
            string[] nLines, jLines;
            try
            {
                nLines = System.IO.File.ReadAllLines(name_Address); 
            }
            catch (FileNotFoundException)
            {
                nLabel.Text = "File not found!"; //journals
                nLabel.ForeColor = Color.Red;
                return;
            }
            catch (Exception)
            {
                nLabel.Text = "Unknown error occured!";
                nLabel.ForeColor = Color.Red;
                return;
            }
            try
            {
                jLines = System.IO.File.ReadAllLines(journal_Address);
            }
            catch(FileNotFoundException)
            {
                jLabel.Text = "File not found!"; //names
                jLabel.ForeColor = Color.Red;
                return;
            }
            catch(Exception)
            {
                jLabel.Text = "Unknown error occured!";
                jLabel.ForeColor = Color.Red;
                return;
            }

            dataGridView2.Rows.Add(Math.Max(jLines.Length, nLines.Length));
            for (int i = 0; i < jLines.Length; i++)
                dataGridView2.Rows[i].Cells["journals"].Value = jLines[i];
            for (int j = 0; j < nLines.Length; j++)
                dataGridView2.Rows[j].Cells["names"].Value = nLines[j];
            jLabel.Text = "Successfully loaded!";
            nLabel.Text = "Successfully loaded!";
            jLabel.ForeColor = Color.Black;
            nLabel.ForeColor = Color.Black;
        }
    }
    /// <summary>
    /// Shuffler class
    /// </summary>
    static class RandomExtensions
    {
        public static void Shuffle<T>(this Random rng, T[] array)
        {
            int n = array.Length;
            while (n > 1)
            {
                int k = rng.Next(n--);
                T temp = array[n];
                array[n] = array[k];
                array[k] = temp;
            }
        }
    }

    // This class is not used
    /// <summary>
    /// Auxilary class to capture screenshot
    /// </summary>
    public class User32
    {
        [StructLayout(LayoutKind.Sequential)]
        public struct Rect
        {
            public int left;
            public int top;
            public int right;
            public int bottom;
        }

        [DllImport("user32.dll")]
        public static extern IntPtr GetWindowRect(IntPtr hWnd, ref Rect rect);
    }
}
